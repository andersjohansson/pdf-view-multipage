;;; pdf-view-multipage.el --- Display multiple pages with pdf-view  -*- lexical-binding: t; -*-

;; Copyright (C) 2020  Anders Johansson

;; Author: Anders Johansson <mejlaandersj@gmail.com>
;; Keywords: files, multimedia

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:


;; For using: Switch to pdf file buffer in two adjacent frames and
;; then enable ‘pdf-view-multipage-mode’.
;;
;; Adapted slightly from:
;; https://gist.github.com/politza/a3339a4f3b3901fcce4c



;;; Code:

(require 'pdf-tools)

(defgroup pdf-view-multipage nil
  "Options for ‘pdf-view-multipage-mode’."
  :group 'pdf-view)

(defcustom pdf-view-multipage-horizontally nil
  "If non-nil, windows are sorted left to right first."
  :type 'boolean)

(defcustom pdf-view-multipage-cycle-pages nil
  "If non-nil, pages are cycled around the windows.
This means that the topmost/leftmost window will start showing
the first page when the bottommost/rightmost window shows the
last.
This makes the ‘pdf-view-multipage-odd-pages-left’ functionality possible."
  :type 'boolean)

(defcustom pdf-view-multipage-odd-pages-left nil
  "If non-nil, make sure odd pages are on the left in two-buffer setups.
Implies ‘pdf-view-multipage-cycle-pages’."
  :type 'boolean)

(defcustom pdf-view-multipage-dedicated t
  "If non-nil, make the windows displaying the file dedicated to avoid reuse by other buffers."
  :type 'boolean)

(defvar pdf-view-multipage-mode-map nil)

(defun pdf-view-multipage--set-mode-map (remap-all)
  "Set ‘pdf-view-multipage-mode-map’, depending on REMAP-ALL’."
  (setq pdf-view-multipage-mode-map
        (let ((kmap (make-sparse-keymap)))
          (set-keymap-parent kmap pdf-view-mode-map)

          (define-key kmap [remap pdf-view-next-page-command]
            #'pdf-view-multipage-next-page)
          (define-key kmap [remap pdf-view-previous-page-command]
            #'pdf-view-multipage-previous-page)

          (when remap-all
            (define-key kmap [remap pdf-view-previous-line-or-previous-page]
              #'pdf-view-multipage-previous-page)
            (define-key kmap [remap pdf-view-next-line-or-next-page]
              #'pdf-view-multipage-next-page)
            (define-key kmap [remap pdf-view-scroll-up-or-next-page]
              #'pdf-view-multipage-next-page)
            (define-key kmap [remap pdf-view-scroll-down-or-previous-page]
              #'pdf-view-multipage-previous-page))
          kmap)))

(defun pdf-view-multipage--set-remap-all (var val)
  "Setter for ‘pdf-view-multipage-remap-all’.
Sets VAR to VAL and set mode map."
  (set-default var val)
  (pdf-view-multipage--set-mode-map val))

(defcustom pdf-view-multipage-remap-all nil
  "If non-nil, remap additional commands to multipage page change commands.
Remaps commands like: ‘pdf-view-next-line-or-next-page’ to
‘pdf-view-multipage-next-page’. This assumes that we always want
to view full pages and newer scroll when we are using
‘pdf-view-multipage-mode’, which seems reasonable."
  :group 'pdf-view
  :type 'boolean
  :set #'pdf-view-multipage--set-remap-all)

;;;###autoload
(define-minor-mode pdf-view-multipage-mode
  "View and navigate multiple pages in all windows.

\\{pdf-view-multipage-mode-map}"
  :keymap pdf-view-multipage-mode-map
  (pdf-util-assert-pdf-buffer)
  (if pdf-view-multipage-mode
      (progn (pdf-view-multipage-adjust-pages)
             (when pdf-view-multipage-dedicated
               (dolist (w (pdf-view-multipage-get-windows))
                 (set-window-dedicated-p w 'not-strongly)))
             (when pdf-view-multipage-remap-all
               (when (boundp 'mwheel-scroll-up-function)
                 (setq-local mwheel-scroll-up-function
                             #'pdf-view-multipage-next-page))
               (when (boundp 'mwheel-scroll-down-function)
                 (setq-local mwheel-scroll-down-function
                             #'pdf-view-multipage-previous-page))))
    (dolist (w pdf-view-multipage-get-windows)
      (set-window-dedicated-p w nil))))

(defun pdf-view-multipage-get-windows (&optional horizontally-p)
  "Return sorted list of windows showing current buffer.
Sorted vertically first, but horizontally first if HORIZONTALLY-P
is non-nil."
  (sort (get-buffer-window-list (current-buffer))
        (lambda (w1 w2)
          (let* ((e1 (window-edges w1))
                 (e2 (window-edges w2))
                 (l1 (car e1)) (t1 (cadr e1))
                 (l2 (car e2)) (t2 (cadr e2)))
            (if horizontally-p
                (or (< t1 t2)
                    (and (= t1 t2)
                         (< l1 l2)))
              (or (< l1 l2)
                  (and (= l1 l2)
                       (< t1 t2))))))))
                
(defun pdf-view-multipage-next-page (&optional arg)
  "Go forward 1 or ARG pages in ‘pdf-view-multipage-mode’."
  (interactive "p")
  (let* ((windows (pdf-view-multipage-get-windows
                   pdf-view-multipage-horizontally))
         (selwin (selected-window))
         (nwin (length windows))
         (pos (cl-position selwin windows))
         (page (pdf-view-current-page))
         (npages (pdf-info-number-of-pages))
         (arg (* arg nwin))
         (op2p (and pdf-view-multipage-odd-pages-left (eq nwin 2))))
    (if (or op2p pdf-view-multipage-cycle-pages)
        (progn
          (cond
           ((eq 0 arg)
            (if op2p ; adjust
                (setq page (- page (mod page 2) 1))
              (setq page (1- page))))
           (t ; increase or decrease
            (setq page (- (+ page arg) pos 1))))
          (dolist (w windows)
            (with-selected-window w
              (setq page (1+ (mod page npages)))
              (pdf-view-goto-page page))))
      (cond
       ((> arg 0)
        (setq arg (min arg (- npages
                              (+ page (- nwin pos 1)))))
        (when (<= arg 0)
          (user-error "End of document"))
        ;; Set page of first window.
        (setq page (- (+ page arg) pos)))
       ((< arg 0)
        (setq arg (max arg (- (- page pos 1))))
        (when (>= arg 0)
          (user-error "Beginning of document"))
        (setq page (- (+ page arg) pos)))
       (t
        ;; Adjust pages in this case, starting at the first window.
        (setq page (max 1
                        (min (with-selected-window (car windows)
                               (pdf-view-current-page))
                             (- npages (1- nwin)))))))
      (dolist (w windows)
        (with-selected-window w
          (pdf-view-goto-page page)
          (setq page (min (1+ page) npages)))))))

(defun pdf-view-multipage-adjust-pages ()
  "Make sure pdf pages in visible buffers are contigous."
  (interactive)
  (pdf-view-multipage-next-page 0))

(defun pdf-view-multipage-previous-page (&optional arg)
  "Go backward 1 or ARG pages in ‘pdf-view-multipage-mode’."
  (interactive "p")
  (pdf-view-multipage-next-page (- (or arg 1))))

(provide 'pdf-view-multipage)

;;; pdf-view-multipage.el ends here
